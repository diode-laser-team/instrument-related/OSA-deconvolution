clear all

%% Import data
opts = delimitedTextImportOptions("NumVariables", 2);
opts.DataLines = [1, Inf];
opts.Delimiter = "\t";
opts.VariableNames = ["lambda", "SP"];
opts.VariableTypes = ["double", "double"];
opts.ExtraColumnsRule = "ignore";
opts.EmptyLineRule = "read";
opts = setvaropts(opts, ["lambda", "SP"], "DecimalSeparator", ",");
tbl = readtable("./meas.txt", opts);
lambda = tbl.lambda;
SP = tbl.SP; % Spectral power
tblref = readtable("./measref.txt", opts);
lambdaref = tblref.lambda;
SPref = tblref.SP;
clear opts tbl tblref

%% 
SPref = 10.^((SPref - max(SPref))/10);
PSF = SPref(find(SPref == max(SPref)) + (-200:200));
lambdaref = lambdaref/1e-9;
deltaLambda_PSF = lambdaref(find(SPref == max(SPref)) + (-200:200)) - lambdaref(SPref == max(SPref));
SP = 10.^((SP - max(SP))/10);
lambda = lambda/1e-9;

figure(1);clf;
subplot(2,3,1);
plot(lambdaref,SPref)
axis tight; grid on; grid minor;
xlabel('\lambda [nm]');
ylabel('Norm. spectral power');
% ylim([-45 2]);
ylim([-0.02 1.02]);
xlim(mean(lambda) + [-0.5 0.5]);
title('Measured reference spectrum');

subplot(2,3,2);
plot(deltaLambda_PSF,PSF)
axis tight; grid on; grid minor;
xlabel('\Delta\lambda [nm]');
ylabel('Norm. spectral power');
% ylim([-45 2]);
ylim([-0.02 1.02]);
title('Point spread function');

subplot(2,3,3);
SPref_deconv = deconvlucy(SPref,PSF);
SPref_deconv = SPref_deconv/max(SPref_deconv);
plot(lambdaref,SPref_deconv)
axis tight; grid on; grid minor;
xlabel('\lambda [nm]');
ylabel('Norm. spectral power');
% ylim([-45 2]);
ylim([-0.02 1.02]);
xlim(mean(lambdaref) + [-0.5 0.5]);
title('Deconvolved reference spectrum');

subplot(2,3,4);
plot(lambda,SP)
axis tight; grid on; grid minor;
xlabel('\lambda [nm]');
ylabel('Norm. spectral power');
% ylim([-45 2]);
ylim([-0.02 1.02]);
xlim(mean(lambda) + [-0.5 0.5]);
title('Measured spectrum');

subplot(2,3,5);
SP_deconv = deconvlucy(SP,PSF);
SP_deconv = SP_deconv/max(SP_deconv);
plot(lambda,SP_deconv)
axis tight; grid on; grid minor;
xlabel('\lambda [nm]');
ylabel('Norm. spectral power');
% ylim([-45 2]);
ylim([-0.02 1.02]);
xlim(mean(lambda) + [-0.5 0.5]);
title('Deconvolved spectrum');

